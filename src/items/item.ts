import {Layer} from "../util/canvas";
import {patternLoadingQueue} from "../util/pattern";

/**
 * Items are like clothing but do not need to be dynamically drawn
 * They do not scale in form with body dimensions, so can be predrawn in svg, png, and other forms
 * Positioning and sizing of an item optionally depends on draw exports (ie. relative to parts of body)
 * May have body modifiers under Mods property
 * @memberof module:da
 */
export class Item {
    public x: number;
    public y: number;
    public layer: number;

    public name: string;
    public src: string;

    constructor(...data) {
        Object.assign(this, {
            layer: Layer.BASE,
        }, ...data);
        if (this.hasOwnProperty("name") === false) {
            throw new Error("Constructing item with no name!");
        }
        if (this.hasOwnProperty("src") === false) {
            throw new Error("Constructing item with no src!");
        }
    }

    /**
     * Return the starting location for drawing the item
     * @param ex Drawing exports
     * @param {number} width
     * @param {number} height
     * @returns {(null|{x: number, y: number})}
     */
    renderItemLocation() {
        if (this.hasOwnProperty("x") && this.hasOwnProperty("y")) {
            return {
                x: this.x,
                y: this.y
            };
        }
        return null;
    }

    /**
     * Modify the Player pose (getPartInLocation inside function and modify pose Mods directly)
     * @this Player
     */
    modifyPose() {
    }

    /**
     * Restore the effect of modifying pose when item was first wielded
     * @this Player
     */
    restorePose() {
    }
}

const cachedItems = {};


export const Items = {
    getItemRender(item) {
        if (cachedItems.hasOwnProperty(item.name)) {
            return cachedItems[item.name];
        }
        throw new Error("Item not loaded yet: " + item.name);
    },

    /**
     * Similar to getItem, load the resource if not cached
     * @param item
     * @param anonOrigin
     */
    loadItem(item, anonOrigin = true) {
        const Items = this;
        if (cachedItems.hasOwnProperty(item.name)) {
            return;
        }

        // create the promise to be loaded before we draw the item
        patternLoadingQueue.push(new Promise<void>((resolve, reject) => {
                const image = new Image();
                image.onload = () => {
                    cachedItems[item.name] = image;
                    resolve();
                };
                image.onerror = () => {
                    console.warn(Error("failed to load " + item.src + ". Retrying with different CORS settings"));
                    reject();
                };

                if (anonOrigin) {
                    image.crossOrigin = "anonymous";
                }
                image.src = item.src;
            }).catch(() => {
                if (anonOrigin) {
                    //Re-attempt without image.crossOrigin = "anonymous" to solve Chrome local issue
                    return Items.loadItem(item, false)
                } else {
                    alert(Error("failed to load " + item.src));
                }
            })
        );
    },

    /**
     * Create a Item instance
     * @memberof module:da.Items
     * @param {Item} Item Item prototype to instantiate
     * @param {object} data Overriding data
     * @returns {Item} Instantiated clothing object
     */
    create(Item, ...data) {
        return new Item(...data);
    }

};
