/**
 * Core stats (where user would define their gameplay stats)
 * Each are statistic descriptions with low, high, average, and stdev (assume normal)
 * @memberof module:da
 */
export const statLimits = {
    fem: {
        desc: "how overall feminine their appearance is; influences a lot of dimensions",
        low  : 0,
        high : 11,
        avg  : 5,
        stdev: 1,
        bias : 2,
    },
    pregnancy: {
        desc: "stages of pregnancy affecting belly protrusion",
        low  : 0,
        high : 10,
        avg  : 0,
        stdev: 0,
        bias : 0,
    },
};

/**
 * Discrete core stats
 * @memberof module:da
 */
export const statDiscretePool = {
    // pool of available values for discrete properties
    skeleton: ["human"],  // underlying racial structure of player
};
