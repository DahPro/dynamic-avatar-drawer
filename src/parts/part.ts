import {
    extractSideLocation,
    extractBaseLocation,
    extractLocationModifier,
    extractUnmodifiedLocation
} from "../util/part";
import {DrawPoint, none} from "drawpoint/dist-esm";
import {Layer} from "../util/canvas";
import {Player} from "../player/player";

/**
 * All parts should go in this namespace
 * @namespace Part
 * @memberof module:da
 */

export class BasePart {
    /**
     * Parent part
     */
    public parentPart: BasePart | null = null;

    /**
     * Location of the part, prepend with + to mean allow other
     * parts to also occupy this location, - to mean restrict any other part from being here
     */
    public loc: string;

    /**
     * Drawing layer
     */
    public layer: number = Layer.BASE;

    /**
     * Whether this part should be drawn on the other side as well
     */
    public reflect: boolean = false;

    /**
     * Whether this part should not be drawn if there are clothes
     * covering its location
     */
    public coverConceal: string[] = [];

    /**
     * Whether it's possible to wear anything over this part
     * (mutually exclusive with converconceal)
     */
    public uncoverable: boolean = false;

    /**
     * List of "{part group} {location}" strings that specify
     * which parts of which part groups this part should be drawn above. For example, having
     * "decorativeParts torso" will ensure this part gets drawn above any parts in that location.
     * Specifying only a location will mean to be above any part in that location regardless of group.
     */
    public aboveParts: string[] = [];

    /**
     * Opposite to aboveParts.
     */
    public belowParts: string[] = [];

    public forcedSide: boolean;

    public side: PartSideNumeric;
    public _owner? : any;
    public Mods?: any;

    // TODO add shared data here
    constructor(...data) {
        Object.assign(this, ...data);
    }

    stroke(ctx?: CanvasRenderingContext2D, ex?: any) {
        return "inherit";
    }

    fill(ctx?: CanvasRenderingContext2D, ex?: any) {
        return "inherit";
    }

    // how thick the stroke line should be
    getLineWidth(avatar?: Player) {
        return 1.5;
    }

    toString() {
        return this.loc;
    }

    /**
     * Calculate drawpoints associated with this part and return it in the sequence to be drawn.
     * @this {object} Calculated dimensions of the player owning the part
     * @param {object} ex Exports from draw that should hold draw points calculated up to now;
     * additional draw points defined by this part should be defined on ex
     * @param {object} mods Combined modifiers of the part and the Player owning the part
     * @param {boolean} calculate
     * @param {module:da.BodyPart} part The body part itself
     * @return {object[]} List of draw points (or convertible to draw point objects)
     */
    calcDrawPoints(ex: any, mods: any, calculate: boolean, part: BasePart) : DrawPoint[] {
        return [];
    }
}

export class BodyPart extends BasePart {
    constructor(...data) {
        super(...data);
    }

    /**
     * Set the stroke pattern for this part
     * @returns {string}
     */
    stroke(ctx?: CanvasRenderingContext2D, ex?: any) {
        return none;
    }

    /**
     * Set the fill pattern for this part
     * @returns {string}
     */
    fill(ctx?: CanvasRenderingContext2D, ex?: any) {
        return "inherit";
    }

    /**
     * Set how thick the stroke line should be
     * @returns {number}
     */
    getLineWidth(avatar?: Player) {
        return 0.8;
    }
}

export interface PartPrototype {
    new(...args: any[]): BasePart;
}

export type PartSideNumeric = 0 | 1;
export type PartSide = null | PartSideNumeric | "left" | "right";
export const Part: { RIGHT: 0, LEFT: 1, create: (prototype: PartPrototype, ...data: any[]) => any } = {
    /**
     * Right side of the body for anything taking side
     * @readonly
     * @type {number}
     */
    RIGHT: 0,
    /**
     * Left side of the body for anything taking side
     * @readonly
     * @type {number}
     */
    LEFT : 1,
    /**
     * Give me a base body part and a side it's supposed to be on
     * I'll return to you a body part specific to that side
     * @memberof module:da.Part
     * @param {BodyPart} PartPrototype Prototype to instantiate with
     * @param {...object} userData Overriding data
     * @returns {BodyPart}
     */
    create(prototype: PartPrototype, ...userData: any[]) {
        const data = Object.assign({}, ...userData);
        let part = new prototype(data);

        let side = (data && data.hasOwnProperty("side")) ? data.side : null;

        // override if part location specifies side
        if (side === null) {
            side = extractSideLocation(part.loc);
        }

        // direct override
        if (part.forcedSide !== undefined) {
            side = part.forcedSide;
        }

        const sideString = getSideLocation(side);

        // rename for more specificity
        if (sideString === "right" || sideString === "left") {
            const baseLocation = extractBaseLocation(part.loc);
            const locationModifiers = extractLocationModifier(part.loc);
            part.loc = locationModifiers + sideString + " " + baseLocation;
        }

        // configure side to be a standard format
        part.side = getSideValue(side);

        return part;
    },
};


/**
 * Get a side string in a well defined format
 * @param side A side string
 * @returns {(string|null)} Either the side string or null if unrecognized
 */
export function getSideLocation(side) : PartSide {
    if (side === "right" || side === "left") {
        return side;
    }
    if (side === Part.LEFT) {
        return "left";
    }
    if (side === Part.RIGHT) {
        return "right";
    }
    return null;
}

export function prototypeAppliesToSide(side, prototype) {
    // no side specifies by default means this prototype applies
    if (side === null) {
        return true;
    }
    // get side of prototype
    const name = prototype.name.toLowerCase();
    if (name.startsWith(side)) {
        return true;
    }
    if (name.startsWith("left") || name.startsWith("right")) {
        return false;
    }
    // if it doesn't start with a side treat it as being shared for all sides
    return true;
}

/**
 * Check whether two parts conflict
 * @param {Part} partA
 * @param {Part} partB
 * @returns {boolean} True if the two parts have conflicting locations
 */
export function partConflict(partA, partB) {
    if (partA.side !== partB.side) {
        return false;
    }
    // else on same side
    if (Object.getPrototypeOf(partA) === Object.getPrototypeOf(partB)) {
        return true;
    }
    if (extractUnmodifiedLocation(partA.loc) === extractUnmodifiedLocation(partB.loc)) {
        // refuse any other part at location
        if (partA.loc.charAt(0) === "-" || partB.loc.charAt(0) === "-") {
            return true;
        }
        // allow any parts at location (so even if same location would still be OK)
        if (partA.loc.charAt(0) === "+" || partB.loc.charAt(0) === "+") {
            return false;
        }
        // otherwise both are unmodified and conflict
        return true;
    } else {
        return false;
    }
}


export function isParentPart(parent, childCandidate) {
    if (!childCandidate.parentPart) {
        return false;
    }
    // first the base part must match
    if (extractBaseLocation(parent.loc) !== childCandidate.parentPart) {
        return false;
    }
    // if the parent has a side and its different than the child
    const parentSide = extractSideLocation(parent.loc);
    return !(parentSide !== null && parentSide !== extractSideLocation(childCandidate.loc));

}

export function getSideValue(side: PartSide): PartSideNumeric {
    if (side === "left" || side === Part.LEFT) {
        return Part.LEFT;
        // could alternatively already be given in numeric terms
    } else {
        return Part.RIGHT;
    }
}

export function getAttachedLocation(partPrototype) {
    const temp = new partPrototype();
    return extractUnmodifiedLocation(temp.loc);
}
