---
layout: post
title:  "1.16 Tattoos"
date:   2018-09-30
categories: release
---

- new display parts: tattoos

## Tattoo Sample

![Nominal tattoos](https://i.imgur.com/sAOI0r8.png)

Tattoos are external images that sit above skin and below any clothing.
You can resize, rotate, and adjust the position of all tattoos.


## Tattoo Definition

Look at the tester source code to see how these tattoos are added, or copy paste the code
generated from the GUI (usage shown below). Remember that the GUI always creates runnable
code and is more reliable than documentation (documentation gets old, but the tester/GUI
will either always work or has a bug that I'll fix upon report)!

### In GUI

<div class="parent" style="height:1000px"><img src="https://i.imgur.com/iULsf7W.gif" class="child"></div>

### In code

{% highlight javascript %}
var tattoo = da.Tattoos.create(da.Tattoo, {
    // layer to display the tattoo on; highly correlated with the location
    layer: da.Layer.ARMS,
    // which body part locations to display the tattoo on
    loc: da.Location.ARM,
    // which side of the body; leave out this property or set to null if it's centered
    // if it's centered, then you should have ignoreClip: true
    side: da.Part.LEFT,
    // give a name to this tattoo so duplicate ones can't be added
    // it also optimizes performance since the image will only need to be loaded once
    name: "dragon",
    // the link (either local or external) to an image to render as the tattoo
    // any image type (jpg, png, svg) will work, but svgs can be kind of wonky
    src: "res/dragon.png",
    // how wide the tattoo is in cm (height scaled to keep aspect ratio)
    width: 40,
    // degrees to rotate counterclockwise
    rotation: 0,
    // set to true only for tattoos at the center of the body
    ignoreClip: false,
    // locate the center of the tattoo given all drawpoints
    // in this case we're moving it 3cm left of the shoulder draw point
    relativeLocation: {
        drawpoint: "shoulder",
        dx: -3,
        dy: 0
    }
});

PC.addTattoo(tattoo);
{% endhighlight %}

