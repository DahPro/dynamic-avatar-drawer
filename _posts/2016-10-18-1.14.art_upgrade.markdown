---
layout: post
title:  "1.14 Art Evolution and New Content"
date:   2016-10-18
categories: release
---

Strokes were removed to give a cleaner and cripser look in version 1.14.
This update also brings customizable eyelashes, a 60FPS transformation
API, and shading fixes.
Versions 1.9 - 1.13 were content updates that will be covered later in
this post.

## Art Evolution

![Crisp art](http://i.imgur.com/M735qne.png)

Shading now distinguishes between cast shadows and core shadows (3 different levels of shading) 

It seems that every update makes the fact that the underlying engine is
2D rather than 3D clearer. Each task that adds detail to upgrade the art
inevitably requires manually encoding the 3D information into drawing parts.

However, I still think it's wise to stick with a 2D engine because:
- 2D is more intuitively to think in
- parameters are 1D, and it's much easier to map 1D -> 2D than 1D -> 3D
- 3D art has a much higher difficulty level/time investment for creating high quality work

Since this library's meant to be used as a tool to create custom assets,
rather than providing all the assets with it, ease of content creation is
vitally important.

## 60FPS Transformation

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/Pu69wDr.gif" class="child"></div>

It's still possible to manipulate parameters directly; this API is just
the recommended way of doing so to get smooth animations with the transformation.

Transformation objects are created with an object to be transformed, a
function to be run during each transformation step, and an object specifying 
the changes (additive to existing parameters).

{% highlight javascript %}
function drawAndExport() {
    da.draw(window.canvasGroup, PC, window.config).then(function (exports) {
        window.ex = exports;
    });
}

var transformation = da.createTransformation(PC, drawAndExport, {
    basedim: {
        armLength: -2,
        faceFem: 1
    }
    Mods: {
        armRotation: 5
    }
    age: 5
});

da.transformAndShow(transformation, 5000);
{% endhighlight %}

`drawAndExport` is the function to be run after each transformation step.
This can be anything; it's just that in the tester we care about the exports
of drawing so we can use it in the GUI.

The third option specifies the parameters to change. It should mirror the
structure of the object to be transformed - Player has base body dimensions
defined under basedim, body modifiers under Mods, and gameplay statistics
directly on the object.

Showing the transformation requires the transformation object and a duration
in milliseconds. It uses requestAnimationFrame, so is only supported in newer browsers.

## New Content
- Eyelash and associated modifiers
- Curved neck and curvature parameters
- New arm shading that extends to hands (if they exist) and cast a more realistically shaped shadow

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/3nEQ6HC.gif" class="child"></div>
- Jackets

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/zfdXPBW.gif" class="child"></div>
- Items (arbitrarily placed assets)

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/XTgsO5w.gif" class="child"></div>
- Socks of various length
- Heels
- Shoes
